﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Net.Sockets;
using System.Net;
using Microsoft.Win32.TaskScheduler;
using NetFwTypeLib;

namespace smart_dziennik
{

    public class Program
    {
        public int xd = 0;
        public static NotifyIcon tray = new NotifyIcon();
        public static void Main(string[] args)
        {
            dodajDoStartupa();
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(czysc);
            Stream st;
            Assembly a = Assembly.GetExecutingAssembly();
            st = a.GetManifestResourceStream("smart_dziennik.Resources.ikona.ico");
            tray.Icon = new Icon(st);
            tray.Visible = true;
            //Nazwa komputera
            wyswietlNazweHosta();
            //aktualnie zalogowani uzytkownicy
            aktualnie_zalogowani();
            //publiczny adress ip
            publiczne_ip();
            //lokalny adres ip
            lokalne_ip();
            //antywirusy
            zainstalowane_antywirusy();
            //firewall
            firewallstatus();
            //metoda do pobierania smarta
            pobierzSmarta();
            //odczytywanie errorow z dziennika zdarzen
            odczyterrorow();
            tray.Visible = false;
        }
        public static void pobierzSmarta()
        {
            ManagementObjectSearcher wyszukiwanie = new ManagementObjectSearcher("Select * from Win32_DiskDrive");
            string[,] dyski_modele = new string[10, 10];
            int licz_dyski = 0;
            foreach (ManagementObject dysk in wyszukiwanie.Get())
            {
                dyski_modele[licz_dyski, 0] = (dysk["Model"]).ToString();
                dyski_modele[licz_dyski, 1] = (dysk["SerialNumber"]).ToString();
                dyski_modele[licz_dyski, 2] = (dysk["Size"]).ToString();
                dyski_modele[licz_dyski, 9] = (dysk["Status"]).ToString();
                licz_dyski++;
            }
            wyszukiwanie.Scope = new ManagementScope(@"\root\wmi");
            wyszukiwanie.Query = new ObjectQuery("Select * from  MSStorageDriver_FailurePredictData");
            ManagementObjectCollection dane_smarta_zbiorcze = wyszukiwanie.Get();
            ManagementObjectCollection Drives = wyszukiwanie.Get();
            Caly_SMART dane_smarta;
            int licznik = 0;
            
            foreach (ManagementObject dane_smarta_osobne in dane_smarta_zbiorcze)
            {
                int licznik_bledow = 0;
                if (licznik < licz_dyski + 1)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("Model dysku: " + dyski_modele[licznik, 0]);
                    Console.WriteLine("Numer seryjny dysku: " + dyski_modele[licznik, 1]);
                    Console.WriteLine("Pojemność dysku (w GB): " + Math.Round(Convert.ToDouble(dyski_modele[licznik, 2]) / 1073741824), 4);
                    if (dyski_modele[licznik, 9] == "OK")
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        dziennikZdarzenInfo(dyski_modele[licznik, 0]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        dziennikZdarzenError(dyski_modele[licznik, 0]);
                    }
                    Console.WriteLine("Status zdrowia dysku (wedlug systemu Windows): " + dyski_modele[licznik, 9]);
                    licznik++;
                }
                Console.ForegroundColor = ConsoleColor.White;
                Byte[] data2 = (Byte[])dane_smarta_osobne.Properties["VendorSpecific"].Value;
                for (int i = 0; i < data2[0] - 1; i++)
                {
                    int start = i * 12;
                    switch (data2[start + 2])
                    {
                        case 1:
                            dane_smarta.RawReadErrorRate.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("RawReadErrorRate: " + dane_smarta.RawReadErrorRate.rawvalue);
                            if (dane_smarta.RawReadErrorRate.rawvalue > 0)
                            {
                                tray.BalloonTipTitle = "Uwaga";
                                licznik_bledow++;
                                tray.BalloonTipText = "Mozliwe problemy z dyskiem " + dyski_modele[licznik, 1];
                                tray.BalloonTipIcon = ToolTipIcon.Error;
                                tray.Visible = true;
                                tray.ShowBalloonTip(3000);
                            }
                            break;
                        case 3:
                            dane_smarta.SpinUpTime.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("SpinUpTime: " + dane_smarta.SpinUpTime.rawvalue);
                            break;
                        case 5:
                            dane_smarta.ReallocatedSectorCount.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("ReallocatedSectorCount: " + dane_smarta.ReallocatedSectorCount.rawvalue);
                            if (dane_smarta.ReallocatedSectorCount.rawvalue > 0)
                            {
                                tray.BalloonTipTitle = "Uwaga";
                                licznik_bledow++;
                                tray.BalloonTipText = "Znaleziono realokacje na dysku!";
                                tray.BalloonTipIcon = ToolTipIcon.Error;
                                tray.Visible = true;
                                tray.ShowBalloonTip(3000);
                            }
                            break;
                        case 10:
                            dane_smarta.SpinRetryCount.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("SpinRetryCount: " + dane_smarta.SpinRetryCount.rawvalue);
                            break;
                        case 191:
                            dane_smarta.GSenseErrorRate.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("G-Sense Error Rate: " + dane_smarta.GSenseErrorRate.rawvalue);
                            break;
                        case 194:
                            dane_smarta.DiskTemperature.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("Disk Temperature " + dane_smarta.DiskTemperature.rawvalue);
                            break;
                        case 196:
                            dane_smarta.ReallocationEventCount.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("ReallocationEventCount: " + dane_smarta.ReallocationEventCount.rawvalue);
                            if (dane_smarta.ReallocationEventCount.rawvalue > 0)
                            {
                                tray.BalloonTipTitle = "Uwaga";
                                licznik_bledow++;
                                tray.BalloonTipText = "Znaleziono zdarzenia realokacji na dysku!";
                                tray.BalloonTipIcon = ToolTipIcon.Error;
                                tray.Visible = true;
                                tray.ShowBalloonTip(3000);
                            }
                            break;
                        case 197:
                            dane_smarta.CurrentPendingSectorCount.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("CurrentPendingSectorCount: " + dane_smarta.CurrentPendingSectorCount.rawvalue);
                            if (dane_smarta.CurrentPendingSectorCount.rawvalue > 0)
                            {
                                tray.BalloonTipTitle = "Uwaga";
                                licznik_bledow++;
                                tray.BalloonTipText = "Znaleziono potencjalnie uszkodzone sektory na dysku: " + (dyski_modele[licznik - 1, 1]) + "!";
                                tray.BalloonTipIcon = ToolTipIcon.Error;
                                tray.Visible = true;
                                tray.ShowBalloonTip(3000);
                            }
                            break;
                        case 198:
                            dane_smarta.OfflineScanUncorrectableSectorCount.rawvalue = data2[start + 7] + 256 * data2[start + 8];
                            Console.WriteLine("OfflineScanUncorrectableSectorCount: " + dane_smarta.OfflineScanUncorrectableSectorCount.rawvalue);
                            break;
                    }
                    if (licznik_bledow == 0)
                        {
                            tray.BalloonTipTitle = "Uwaga";
                            tray.BalloonTipText = "Dysk " + (dyski_modele[licznik - 1, 1]) + "sprawdzony. Brak bledow!";
                            tray.BalloonTipIcon = ToolTipIcon.Info;
                            tray.Visible = true;
                            tray.ShowBalloonTip(3000);
                        }
                    else
                        {
                            licznik_bledow = 0;
                        }
                }
                Console.WriteLine("====================================================================");
            }
            Console.WriteLine("Kliknij ENTER, aby przejsc do odczytu bledow z dziennika zdarzen");
            Console.ReadLine();
        }
        public struct Atrybuty_SMARTA
        {
            public int rawvalue;
        }
        public struct Caly_SMART
        {
            public Atrybuty_SMARTA RawReadErrorRate;
            public Atrybuty_SMARTA SpinUpTime;
            public Atrybuty_SMARTA SpinRetryCount;
            public Atrybuty_SMARTA ReallocatedSectorCount;
            public Atrybuty_SMARTA GSenseErrorRate;
            public Atrybuty_SMARTA DiskTemperature;
            public Atrybuty_SMARTA ReallocationEventCount;
            public Atrybuty_SMARTA CurrentPendingSectorCount;
            public Atrybuty_SMARTA OfflineScanUncorrectableSectorCount;
        }
        public static void dziennikZdarzenError(string Dysk_nazwa)
        {
            string sourceName = "System";
            EventLog eventLog = new EventLog();
            eventLog.Source = sourceName;
            eventLog.WriteEntry("Dysk " + Dysk_nazwa + " jest uszkodzony. Sprawdź go!", EventLogEntryType.Error);
        }
        public static void dziennikZdarzenInfo(string Dysk_nazwa)
        {
            string sourceName = "System";
            EventLog eventLog = new EventLog();
            eventLog.Source = sourceName;
            eventLog.WriteEntry("Dysk " + Dysk_nazwa + " jest ok", EventLogEntryType.Information);
        }
        public static void odczyterrorow()
        {
            Console.WriteLine("Odczytywanie errorow z dziennika zdarzeń");
            string nazwa = "System";
            EventLog logi = new EventLog(nazwa);
            EventLogEntryCollection wystapienia = logi.Entries;
            foreach (EventLogEntry wystapienie in wystapienia)
            {
                if (wystapienie.EntryType == EventLogEntryType.Error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Poziom zdarzenia: {0}", wystapienie.EntryType);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Identyfikator zdarzenia: {0}", wystapienie.InstanceId);
                    Console.WriteLine("Wiadomosc: {0}", wystapienie.Message);
                    Console.WriteLine("Zrodlo: {0}", wystapienie.Source);
                    Console.WriteLine("Data: {0}", wystapienie.TimeGenerated);
                    Console.WriteLine("###################################");
                }
                //else if (wystapienie.EntryType == EventLogEntryType.Warning)
                //    {
                //        Console.ForegroundColor = ConsoleColor.Yellow;
                //        Console.WriteLine("Poziom zdarzenia: {0}", wystapienie.EntryType);
                //        Console.ForegroundColor = ConsoleColor.White;
                //        Console.WriteLine("Identyfikator zdarzenia: {0}", wystapienie.InstanceId);
                //        Console.WriteLine("Wiadomosc: {0}", wystapienie.Message);
                //        Console.WriteLine("Zrodlo: {0}", wystapienie.Source);
                //        Console.WriteLine("Data: {0}", wystapienie.TimeGenerated);
                //        Console.WriteLine("###################################");
                //    }
            }
        }
        static void czysc(object sender, EventArgs e)
        {
            tray.Visible = false;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public static void publiczne_ip()
        {
            Console.WriteLine("==============================");
            string pubIp = new System.Net.WebClient().DownloadString("https://api.ipify.org");
            Console.WriteLine("Mój publiczny adres IP to: " + pubIp);
        }
        public static void lokalne_ip()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            int licznik = 0;
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    licznik++;
                    Console.WriteLine("Lokalny adres IP numer " + licznik + ": " + ip.ToString());
                }
            }
            Console.WriteLine("==============================");
        }
        public static void wyswietlNazweHosta()
        {

            Console.WriteLine("Nazwa komputera to: " + Environment.MachineName);
        }
        public static void aktualnie_zalogowani()
        {
            //ProcessStartInfo psi = new ProcessStartInfo("cmd");
            //psi.UseShellExecute = false;
            //psi.RedirectStandardOutput = true;
            //psi.CreateNoWindow = true;
            //psi.RedirectStandardInput = true;
            //var proc = Process.Start(psi);
            //proc.StandardInput.WriteLine(@"query user");
            //proc.StandardInput.WriteLine("exit");
            //string s = proc.StandardOutput.ReadToEnd();
            //Console.Write(s);
            Console.WriteLine("Aktualnie zalogowany user: {0}", Environment.UserName);
        }
        public static void zainstalowane_antywirusy()
        {
            Console.WriteLine("==============Antywirusy==============");
            ManagementObjectSearcher wmiData = new ManagementObjectSearcher(@"root\SecurityCenter2", "SELECT * FROM AntiVirusProduct");
            ManagementObjectCollection data = wmiData.Get();

            foreach (ManagementObject virusChecker in data)
            {
                var virusCheckerName = virusChecker["displayName"];
                var virusCheckerStatus = virusChecker["productState"];
                if (virusCheckerStatus.ToString() == "266240")
                    {
                        virusCheckerStatus = "Aktualnie dziala";
                    }
                else if (virusCheckerStatus.ToString() == "397584")
                    {
                        virusCheckerStatus = "Aktualnie dziala, ale jest nieaktualny";
                    }
                else if (virusCheckerStatus.ToString() == "397568")
                    {
                        virusCheckerStatus = "Aktualnie dziala i jest aktualny";
                    }
                else
                    {
                        virusCheckerStatus = "Status nieznany";
                    }
                Console.WriteLine("Nazwa: " + virusCheckerName + " Status: " + virusCheckerStatus);
            }
            Console.WriteLine("====================================");
        }
        public static void dodajDoStartupa()
        {
            var Startup = Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            if (!File.Exists("C:\\smart_dziennik.exe") && !File.Exists("C:\\Dysk_testowy.xml"))
            {
                WriteResourceToFile("smart_dziennik.Resources.Dysk_testowy.xml", "C:\\Dysk_testowy.xml");
                File.Copy("smart_dziennik.exe", "C:\\smart_dziennik.exe");
                string komenda = "schtasks /create /xml \"C:\\Dysk_testowy.xml\" /tn \"Dysk testowy\" /ru %COMPUTERNAME%\\%username%";
                System.Diagnostics.Process.Start("CMD.exe", "/C " + komenda);
            }
        }
        public static void WriteResourceToFile(string resourceName, string fileName)
        {
            using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
            {
                using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    resource.CopyTo(file);
                }
            }
        }
        //firewall 
        public static void firewallstatus()
        {
            Type NetFwMgrType = Type.GetTypeFromProgID("HNetCfg.FwMgr", false);
            INetFwMgr mgr = (INetFwMgr)Activator.CreateInstance(NetFwMgrType);
            bool Firewallenabled = mgr.LocalPolicy.CurrentProfile.FirewallEnabled;
            if (Firewallenabled == true)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Firewall wlaczony");
                }
            else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Firewall wylaczony");
                }
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("=====================================");
        }
    }
}
